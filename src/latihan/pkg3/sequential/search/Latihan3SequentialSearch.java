/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan.pkg3.sequential.search;

import javax.swing.JOptionPane;

public class Latihan3SequentialSearch {
    
    public static void main(String[] args) {
        int data [] = new int [5];
        int i, angka, cari;
        boolean ketemu;
        
        for (i=0; i<data.length; i++) {
            angka = Integer.parseInt (JOptionPane.showInputDialog("masukkan data ke- " + (i+1)));
            data [i] = angka;
        }
        
        System.out.print("angka dimasukkan = { ");
        for (i=0; i<data.length; i++){
            System.out.print(data[i] + "");
        }
        System.out.println ("}");
        cari = Integer.parseInt (JOptionPane.showInputDialog("masukkan angka yang ingin dicari :"));
        System.out.println ("angka dicari : " +cari);
        ketemu = false;
        for (i = 0; i<data.length; i++){
            if (data [i] == cari) {
                ketemu=true;
                break;
            }
        }
        if (ketemu) {
            System.out.println("angka ditemukan dalam urutan ke - " + (i+1));
        } else {
            System.out.println ("angka tidak ditemukan ");
        }
    }
    
}
